## argo-hub
**How to add a new application to argo-hub**

In order to add a new application to the argo-hub one will need to create a new folder under the cd folder with the name of the that application. In that folder, he'll need to create at least three files:

- kustomization.yaml - this file is needed by the kustomize tool and its content should contain the resources needed for the application.
- application.yaml - this file will include the URL to the 3rdParty application and default values for the 3rdParty parameters
- namespace.yaml - this file will only have the default namespace name for the 3rdParty application

Here are some examples:
#istio/kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
  
resources:
  - istio-namespace.yaml
  - istio-system-namespace.yaml
  - application.yaml


#istio/application.yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: istio
  namespace: argocd
spec:
  syncPolicy:
    automated: {}
  destination:
    namespace: istio
    server: https://kubernetes.default.svc
  project: default
  source:
    path: manifests/charts/base
    repoURL: https://github.com/istio/istio.git
    targetRevision: 1.6.5



#istio/istio-namespace.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: istio



#istio/istio-system-namespace.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: istio-system




For example if you need to pass values to the 3rdParty application, you can do that as in the example below
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: chaoskube
  namespace: argocd
spec:
  syncPolicy:
    automated: {}
  destination:
    namespace: chaoskube
    server: https://kubernetes.default.svc
  project: default
  source:
**    helm:
      values: |
        rbac:
          create: true**


    chart: chaoskube
    repoURL: https://kubernetes-charts.storage.googleapis.com
    targetRevision: 3.1.4